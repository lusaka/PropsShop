local prop_shops = {
	{ ['x'] = 2748.787109375, ['y'] = 3472.63647460938, ['z'] = 55.6771392822266 },
}
prop = {
	[49] = {
		model='prop_beach_fire',
		z="0.1",
	},
	[50] = {
		model='prop_bong_01',
		z="0.0",
	},
	[51] = {
		model='prop_food_van_02',
		z="1.2",
	},
	[52] = {
		model='prop_speaker_07',
		z="0.45",
	},
	[53] = {
		model='prop_jetski_ramp_01',
		z="1.0",
	},
	[54] = {
		model='prop_barrier_work04a',
		z="0.0",
	},
	[55] = {
		model='prop_mp_cone_02',
		z="0.0",
	},
	[56] = {
		model='prop_patio_lounger1b',
		z="0.0",
	},
	[57] = {
		model='prop_patio_lounger1_table',
		z="0.2",
	},
	[58] = {
		model='prop_toolchest_02',
		z="0.0",
	},
	[59] = {
		model='prop_t_sofa',
		z="0.32",
	},
	[60] = {
		model='p_ld_stinger_s',
		z="0.02",
	},
	[61] = {
		model='prop_table_03',
		z="0.4",
	},
	[62] = {
		model='prop_tv_cam_02',
		z="1.0",
	},
	[63] = {
		model='v_club_roc_micstd',
		z="0.45",
	},
	[64] = {
		model='stt_prop_track_slowdown',
		z="0.0",
	},
	[65] = {
		model='prop_barrel_exp_01a',
		z="0.5",
	},
	[66] = {
		model='prop_barrier_work05',
		z="0.0",
	},
	[67] = {
		model='prop_bbq_1',
		z="0.0",
	},
	[68] = {
		model='prop_parasol_01_b',
		z="0.0",
	},
	[69] = {
		model='prop_offroad_barrel02',
		z="0.5",
	},
	[70] = {
		model='prop_gazebo_02',
		z="0.0",
	},	
}



RegisterNetEvent("prop:getProp")
RegisterNetEvent("prop:selection")
RegisterNetEvent("prop:placeme")
RegisterNetEvent("prop:lastProp")
RegisterNetEvent("prop:allProp")

function Chat(t)
	TriggerEvent("chatMessage", 'Shop', { 0, 255, 255}, "" .. tostring(t))
end

Citizen.CreateThread(function()
	for k,v in ipairs(prop_shops)do
		local blip = AddBlipForCoord(v.x, v.y, v.z)
		SetBlipSprite(blip, 52)
		SetBlipScale(blip, 1.2)
		SetBlipColour(blip,17)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Magasin d'objets")
		EndTextCommandSetBlipName(blip)
	end
end)


AddEventHandler("prop:selection", function(data)
	local item = data.itemid
	local price = data.price
	if (exports['vdk_inventory']:notFull() == true) then
		TriggerServerEvent('prop:testprix',item,price)
	else
		TriggerEvent("es_freeroam:notify","CHAR_BANK_MAZE", 1, "Maze Bank", false, "Inventaire Plein")
	end
end)

AddEventHandler("prop:getProp", function(item)
	TriggerEvent('player:receiveItem',item,1)
end)

local propslist = {}

AddEventHandler("prop:placeme", function(itemId)
	if IsPedInAnyVehicle(GetPlayerPed(-1), true) == false then
		if(#propslist < 20) then
			TriggerEvent('player:looseItem', itemId, 1)
			Citizen.CreateThread(function()
				local prophash = GetHashKey(prop[itemId].model)
				RequestModel(prophash)
				while not HasModelLoaded(prophash) do
					Citizen.Wait(0)
				end
				local offset = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 0.75, 0.0)
				local _, worldZ = GetGroundZFor_3dCoord(offset.x, offset.y, offset.z)
				local propsobj = CreateObjectNoOffset(prophash, offset.x, offset.y, worldZ+prop[itemId].z, true, true, true)
				local heading = GetEntityHeading(GetPlayerPed(-1))
				SetEntityHeading(propsobj, heading)
				SetModelAsNoLongerNeeded(prophash)
				--PlaceObjectOnGroundProperly(propsobj)
				SetEntityAsMissionEntity(propsobj)
				propslist[#propslist+1] = ObjToNet(propsobj)
			end)
		else
			DisplayHelpText("Tu ne peux poser que 10 Items Max")
		end
	else
		DisplayHelpText("Et si tu descendais du vehicule ca serai pas mieux ?")
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		local pos = GetEntityCoords(GetPlayerPed(-1), false)
		for k,v in ipairs(prop_shops) do
			if(Vdist(v.x, v.y, v.z, pos.x, pos.y, pos.z) < 20.0)then
				DrawMarker(1, v.x, v.y, v.z - 1, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.5001, 0, 25, 165, 165, 0,0, 0,0)
				if(Vdist(v.x, v.y, v.z, pos.x, pos.y, pos.z) < 1.0)then
					DisplayHelpText("Appuyer sur ~INPUT_CONTEXT~ pour ouvrir/fermer le shop")
					if(IsControlJustReleased(1, 51))then
				        Menu.initMenu()
						Menu.isOpen = not Menu.isOpen
					end
					if Menu.isOpen then
						Menu.draw()
						Menu.keyControl()
					end
				end
			end
		end
	end
end)

-- Citizen.CreateThread(function()
	-- while true do
		-- Citizen.Wait(0)
				
	-- end
-- end)

AddEventHandler("prop:lastProp", function()
	DeleteObject(NetToObj(propslist[#propslist]))
	propslist[#propslist] = nil
end)

AddEventHandler("prop:allProp", function()
	for i, props in pairs(propslist) do
		DeleteObject(NetToObj(props))
		propslist[i] = nil
	end
end)

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(5)
		for _, props in pairs(propslist) do
			local ox, oy, oz = table.unpack(GetEntityCoords(NetToObj(props), true))
			local cVeh = GetClosestVehicle(ox, oy, oz, 20.0, 0, 70)
			if(IsEntityAVehicle(cVeh)) then
				if IsEntityAtEntity(cVeh, NetToObj(props), 20.0, 20.0, 2.0, 0, 1, 0) then
					local cDriver = GetPedInVehicleSeat(cVeh, -1)
					TaskVehicleTempAction(cDriver, cVeh, 6, 1000)
					--SetVehicleHandbrake(cVeh, false)
					SetVehicleIndicatorLights(cVeh, 0, true)
					SetVehicleIndicatorLights(cVeh, 1, true)
				end
			end
		end
	end
end)