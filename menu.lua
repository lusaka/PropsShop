--====================================================================================
-- #Author: Jonathan D @ Gannon
-- 
-- Développée pour la communauté n3mtv
--      https://www.twitch.tv/n3mtv
--      https://twitter.com/n3m_tv
--      https://www.facebook.com/lan3mtv
--====================================================================================





--===================================================================================================================================================--
--                Build Menu -- playAmination = joue l'aniamtion une fois et playAminationLoop pour jouer l'animation en boucle.                     -- 
-- site des emotes = http://docs.ragepluginhook.net/html/62951c37-a440-478c-b389-c471230ddfc5.htm#amb@code_human_wander_idles_cop@male@staticSection --
--===================================================================================================================================================--
Menu = {}
Menu.item = {
    ['Title'] = 'Magasin',
    ['Items'] = {
        {['Title'] = 'Feu de Camp 300$', ['Event'] = 'prop:selection',['itemid']=49,['price']=300,['Close']= false},
		{['Title'] = 'Bong 400$', ['Event'] = 'prop:selection',['itemid']=50,['price']=400,['Close']= false},
		{['Title'] = 'Barak à Frites 2000$', ['Event'] = 'prop:selection',['itemid']=51,['price']=2000,['Close']= false},
		{['Title'] = 'Sono Portable 400$', ['Event'] = 'prop:selection',['itemid']=52,['price']=400,['Close']= false},
		{['Title'] = 'Tremplin 2000$', ['Event'] = 'prop:selection',['itemid']=53,['price']=2000,['Close']= false},
		{['Title'] = 'Barriere de Construction 600$', ['Event'] = 'prop:selection',['itemid']=54,['price']=600,['Close']= false},
		{['Title'] = 'Cone 200$', ['Event'] = 'prop:selection',['itemid']=55,['price']=200,['Close']= false},
		{['Title'] = 'Chaisse Longue 600$', ['Event'] = 'prop:selection',['itemid']=56,['price']=600,['Close']= false},
		{['Title'] = 'Petite Table 300$', ['Event'] = 'prop:selection',['itemid']=57,['price']=300,['Close']= false},
		{['Title'] = 'Boite à Outils 400$', ['Event'] = 'prop:selection',['itemid']=58,['price']=400,['Close']= false},
		{['Title'] = 'Sofa Lounge 1200$', ['Event'] = 'prop:selection',['itemid']=59,['price']=1200,['Close']= false},
		{['Title'] = 'Herse 65000$', ['Event'] = 'prop:selection',['itemid']=60,['price']=65000,['Close']= false},
		{['Title'] = 'Table 450$', ['Event'] = 'prop:selection',['itemid']=61,['price']=450,['Close']= false},
		{['Title'] = 'Camera TV de Camp 2000$', ['Event'] = 'prop:selection',['itemid']=62,['price']=2000,['Close']= false},
		{['Title'] = 'Micro 650$', ['Event'] = 'prop:selection',['itemid']=63,['price']=650,['Close']= false},
		{['Title'] = 'Ralentisseur 5000$', ['Event'] = 'prop:selection',['itemid']=64,['price']=5000,['Close']= false},
		{['Title'] = 'Bidon Explosif 2000$', ['Event'] = 'prop:selection',['itemid']=65,['price']=2000,['Close']= false},
		-- {['Title'] = 'Barriere de Police 400$', ['Event'] = 'prop:selection',['itemid']=66,['price']=400,['Close']= false},
		{['Title'] = 'BBQ 600$', ['Event'] = 'prop:selection',['itemid']=67,['price']=600,['Close']= false},
		{['Title'] = 'Parassol 500$', ['Event'] = 'prop:selection',['itemid']=68,['price']=500,['Close']= false},
		{['Title'] = 'Bidons Directionnels 400$', ['Event'] = 'prop:selection',['itemid']=69,['price']=400,['Close']= false},
		{['Title'] = 'Tente de Plage 600$', ['Event'] = 'prop:selection',['itemid']=70,['price']=600,['Close']= false},
		
    }
}
--====================================================================================
--  Option Menu
--====================================================================================
Menu.backgroundColor = { 52, 73, 94, 196 }
Menu.backgroundColorActive = { 22, 160, 134, 255 }
Menu.tileTextColor = { 22, 160, 134, 255 }
Menu.tileBackgroundColor = { 255,255,255, 255 }
Menu.textColor = { 255,255,255,255 }
Menu.textColorActive = { 255,255,255, 255 }

Menu.keyOpenMenu = 170 -- F3    
Menu.keyUp = 172 -- PhoneUp
Menu.keyDown = 173 -- PhoneDown
Menu.keyLeft = 174 -- PhoneLeft || Not use next release Maybe 
Menu.keyRight =	175 -- PhoneRigth || Not use next release Maybe 
Menu.keySelect = 176 -- PhoneSelect
Menu.KeyCancel = 177 -- PhoneCancel

Menu.posX = 0.15
Menu.posY = 0.1

Menu.ItemWidth = 0.20
Menu.ItemHeight = 0.03

Menu.isOpen = false   -- /!\ Ne pas toucher
Menu.currentPos = {1} -- /!\ Ne pas toucher

--====================================================================================
--  Menu System
--====================================================================================

function Menu.drawRect(posX, posY, width, heigh, color)
    DrawRect(posX + width / 2, posY + heigh / 2, width, heigh, color[1], color[2], color[3], color[4])
end

function Menu.initText(textColor, font, scale)
    font = font or 0
    scale = scale or 0.35
    SetTextFont(font)
    SetTextScale(0.0,scale)
    SetTextCentre(true)
    SetTextDropShadow(0, 0, 0, 0, 0)
    SetTextEdge(0, 0, 0, 0, 0)
    SetTextColour(textColor[1], textColor[2], textColor[3], textColor[4])
    SetTextEntry("STRING")
end

function Menu.draw() 
    -- Draw Rect
    local pos = 0
    local menu = Menu.getCurrentMenu()
    local selectValue = Menu.currentPos[#Menu.currentPos]
    local nbItem = #menu.Items
    -- draw background title & title
    Menu.drawRect(Menu.posX, Menu.posY , Menu.ItemWidth, Menu.ItemHeight * 2, Menu.tileBackgroundColor)    
    Menu.initText(Menu.tileTextColor, 4, 0.7)
    AddTextComponentString(menu.Title)
    DrawText(Menu.posX + Menu.ItemWidth/2, Menu.posY)

    -- draw bakcground items
    Menu.drawRect(Menu.posX, Menu.posY + Menu.ItemHeight * 2, Menu.ItemWidth, Menu.ItemHeight + (nbItem-1)*Menu.ItemHeight, Menu.backgroundColor)
    -- draw all items
    for pos, value in pairs(menu.Items) do
        if pos == selectValue then
            Menu.drawRect(Menu.posX, Menu.posY + Menu.ItemHeight * (1+pos), Menu.ItemWidth, Menu.ItemHeight, Menu.backgroundColorActive)
            Menu.initText(Menu.textColorActive)
        else
            Menu.initText(Menu.textColor)
        end
        AddTextComponentString(value.Title)
        DrawText(Menu.posX + Menu.ItemWidth/2, Menu.posY + Menu.ItemHeight * (pos+1))
    end
    
end

function Menu.getCurrentMenu()
    local currentMenu = Menu.item
    for i=1, #Menu.currentPos - 1 do
        local val = Menu.currentPos[i]
        currentMenu = currentMenu.Items[val].SubMenu
    end
    return currentMenu
end

function Menu.initMenu()
    Menu.currentPos = {1}
end

function Menu.keyControl()
    if IsControlJustPressed(1, Menu.keyDown) then 
        local cMenu = Menu.getCurrentMenu()
        local size = #cMenu.Items
        local slcp = #Menu.currentPos
        Menu.currentPos[slcp] = (Menu.currentPos[slcp] % size) + 1

    elseif IsControlJustPressed(1, Menu.keyUp) then 
        local cMenu = Menu.getCurrentMenu()
        local size = #cMenu.Items
        local slcp = #Menu.currentPos
        Menu.currentPos[slcp] = ((Menu.currentPos[slcp] - 2 + size) % size) + 1

    elseif IsControlJustPressed(1, Menu.KeyCancel) then 
        table.remove(Menu.currentPos)
        if #Menu.currentPos == 0 then
            Menu.isOpen = false 
        end
	elseif IsControlJustPressed(1, 177) or IsControlJustPressed(1, 170) or IsControlJustPressed(1, 166) or IsControlJustPressed(1, 288) or IsControlJustPressed(1, 289) or IsControlJustPressed(1, 311) or IsControlJustPressed(1, 182) then
		Menu.isOpen = false
    elseif IsControlJustPressed(1, Menu.keySelect)  then
        local cSelect = Menu.currentPos[#Menu.currentPos]
        local cMenu = Menu.getCurrentMenu()
        if cMenu.Items[cSelect].SubMenu ~= nil then
            Menu.currentPos[#Menu.currentPos + 1] = 1
        else
            if cMenu.Items[cSelect].Function ~= nil then
                cMenu.Items[cSelect].Function(cMenu.Items[cSelect])
            end
            if cMenu.Items[cSelect].Event ~= nil then
                TriggerEvent(cMenu.Items[cSelect].Event, cMenu.Items[cSelect])
            end
            if cMenu.Items[cSelect].Close == nil or cMenu.Items[cSelect].Close == true then
                Menu.isOpen = false
            end
        end
    end

end

